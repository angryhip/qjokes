# -*- coding: utf-8 -*-
#Grigory Smirnov 2013
#There use sevenid modules (parsing)
#This code is protected by copyright. Copying is prohibited! Distribution of the source code is prohibited!
from tkinter import *
import sqlite3
from urllib.request import Request, urlopen

#Надо что-то сделать с классом ниже, чтобы работало все по старому
class parser():
    def replace(self, line, repl, replto):
        nin2 = len(repl)
        nin1 = line.find(repl)
        while nin1 != -1:
            nin3 = nin1 + nin2
            line = line[:nin1]+replto+line[nin3:]
            nin1 = line.find(repl)
        return(line)
    def GetRequest(self, url):
        requ = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        html = urlopen(requ).read()
        html = html.decode("cp1251")
        return(html)
    def GetEndSaved(self):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone() 
        try:
            self.QuotesNumber=self.lastid[0]
            return(self.lastid[0])
        except:
            return('Пусто')
    def GetQuoteSaved(self, number):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone() 
        try:
            self.QuotesNumber=self.lastid[0]
        except:
            return('Пусто')
        try:
            c.execute('SELECT text FROM quotes WHERE id=?', (str(number)))
            self.tmp=c.fetchone()  
        except:
            g.but_quotes_next.destroy() 
            g.but_quotes_back.destroy() 
            g.but_quotes_begin=Button(g.show_qu_saved, text='Сначала', width=8,height=1, bg="white",fg="black", command=lambda: pars.FirstQuoteSaved())
            g.but_quotes_begin.pack()
            g.but_quote_delete.destroy()
            return('Конец... Нажми кнопку "Сначала, чтобы все увидеть заново."')
        if self.tmp == None:
            g.but_quotes_next.destroy() 
            g.but_quotes_back.destroy() 
            g.but_quotes_begin=Button(g.show_qu_saved, text='Сначала', width=8,height=1, bg="white",fg="black", command=lambda: pars.FirstQuoteSaved())
            g.but_quotes_begin.pack()
            g.but_quote_delete.destroy()
            return('Конец... Нажми кнопку "Сначала, чтобы все увидеть заново."')
        return(self.tmp[0])
    def FirstQuoteSaved(self):
        g.but_quotes_begin.destroy()
        g.but_quotes_back=Button(g.show_qu_saved, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: g.back_quote_saved())
        g.but_quotes_next=Button(g.show_qu_saved, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: g.next_quote_saved())
        g.but_quote_delete=Button(g.show_qu_saved, text='Удалить', width=8,height=1, bg="white",fg="black", command=lambda: g.delete_quote_saved())
        g.but_quotes_back.place(x=100, y=366)
        g.but_quotes_next.place(x=300, y=366)
        g.but_quote_delete.place(x=400, y=366)
        g.quote_saved.delete(1.0, END)
        g.quote_saved.insert(1.0, s.choose_quote_saved(2, pars.GetEndSaved())) 
    def GetEndBASH_IM(self):
        self.html = self.GetRequest('http://bash.im')
        self.line = self.html.split('\n', 31)[30]
        self.line = self.line[83:-31]
        return(self.line)
    def GetEndAnekdotnow(self):
        self.line = (self.GetRequest('http://anekdotnow.ru')).decode("cp1251")
        self.line = self.line[(self.line.find("<td><img src='img/forum.jpg' height='16'></td>\n<td ><a href='/funny.php?t=papa-papa-day-deneg")+93):]
        self.line = self.line[(self.line.find("&k=")+3):]
        self.line = self.line[:(self.line.find("'>"))]
        self.line = int(self.line)
        return(self.line)
    def GetEndITHAPPENS_RU(self):
        self.line = (self.GetRequest('http://ithappens.ru/rss/').decode("cp1251"))
        self.line = self.line.split('\n', 13)[12]
        self.line = self.line[54:(self.line.find("</guid>"))]
        return(int(self.line))
    def GetJokeAnekdotnow(self, number):
        self.line = self.GetRequest('http://anekdotnow.ru/funny.php?k=' + str(self.line)).decode("cp1251")
        self.line = self.line[(self.line.find("<br><center><img src='img/bullet2.gif'></center><br>")+53):(self.line.find("<br clear='all'>"))]
        self.line = self.replace(self.line, '\t'*4, ' ')
        self.line = ' '+self.line[6:]
        self.line = self.replace(self.line, '<br />', '')
        self.line = self.replace(self.line, '&quot;', '"')
        return(self.line)
    def GetQuoteBASH_IM(self, nquote):
        self.line = self.GetRequest('http://bash.im/quote/' + str(nquote))
        self.line = self.line.split('\n', 9)[8]
        self.line = self.line[43:-4]
        self.line = self.replace(self.line, '&#13;&#10;', '\n')
        self.line = self.replace(self.line, '&quot;', '"')
        return(self.line) 
    def GetQuoteITHAPPENS_RU(self, nquote): #nquote - номер цитаты
        self.line = self.line[(self.line.find('<p class="text" id="')+20):]
        self.line = self.line[(self.line.find('">')+2):]
        self.line = self.line[:(self.line.find('</p>'))]
        self.line = self.replace(self.line, '&nbsp;', ' ')
        self.line = self.replace(self.line, '<br>', '\n')
        return(self.line)
class choose():
    def choose_quote_bash(self, back, number):
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            if pars.GetQuoteBASH_IM(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndBASH_IM()
                return(pars.GetQuoteBASH_IM(int(self.number)))
            else:
                return(pars.GetQuoteBASH_IM(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            if pars.GetQuoteBASH_IM(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndBASH_IM()
                return(pars.GetQuoteBASH_IM(int(self.number)))
                g.but_quote_back.destroy()
                g.but_quote_back.pack()
            else:
                return(pars.GetQuoteBASH_IM(int(self.number)))
        elif back==2:
            number=pars.GetEndBASH_IM()
            return(pars.GetQuoteBASH_IM(number))
    def choose_joke_aneknow(self, back, number):
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            if pars.GetJokeAnekdotnow(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndAnekdotnow()
                return(pars.GetJokeAnekdotnow(int(self.number)))
            else:
                return(pars.GetJokeAnekdotnow(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            if pars.GetJokeAnekdotnow(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndAnekdotnow()
                return(pars.GetJokeAnekdotnow(int(self.number)))
                g.but_joke_back.destroy()
                g.but_joke_back.pack()
            else:
                return(pars.GetJokeAnekdotnow(int(self.number)))
        elif back==2:
            number=pars.GetEndAnekdotnow()
            return(pars.GetJokeAnekdotnow(number))
    def choose_quote_ithapp(self, back, number):
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            if pars.GetQuoteITHAPPENS_RU(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndITHAPPENS_RU()
                return(pars.GetQuoteITHAPPENS_RU(int(self.number)))
            else:
                return(pars.GetQuoteITHAPPENS_RU(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            if pars.GetQuoteITHAPPENS_RU(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndITHAPPENS_RU()
                return(pars.GetQuoteITHAPPENS_RU(int(self.number)))
                g.but_quote_back.destroy()
                g.but_quote_back.pack()
            else:
                return(pars.GetQuoteITHAPPENS_RU(int(self.number)))
        elif back==2:
            number=pars.GetEndITHAPPENS_RU()
            return(pars.GetQuoteITHAPPENS_RU(number))
    def choose_quote_saved(self, back, number): 
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            return(pars.GetQuoteSaved(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            return(pars.GetQuoteSaved(int(self.number)))
        elif back==2:
            g.quote_saved.delete(1.0, END)
            number=pars.GetEndSaved()
            return(pars.GetQuoteSaved(number))
                
                

            

class gui():
    def what_window(self, what): #0 - цитаты, 1 - шутки, 2 - о программе
        root.destroy() #Удаляем root окно
        self.show_win=Tk()
        self.show_win.title('Цитаты')
        self.show_win.geometry('300x200+300+200') 
        if what==0:
            quote_bash_but=Button(self.show_win, text='Bash.im', width=8,height=2, bg="white",fg="black", command=lambda: self.show_bash_quote())
            quote_ithapp_but=Button(self.show_win, text='IThappens', width=8,height=2, bg="white",fg="black", command=lambda: self.show_ithapp_quote()) 
            quote_betaa_but=Button(self.show_win, text='AnekdotNow (beta!)', width=8,height=2, bg="white",fg="black", command=lambda: self.show_aneknow_joke()) 
            quote_saved_but=Button(self.show_win, text='Saved', width=8,height=2, bg="white",fg="black", command=lambda: self.show_saved_quote())
            quote_bash_but.pack()
            quote_ithapp_but.pack()
            quote_saved_but.pack()
        if what==1:
            joke_aneknow_but=Button(self.show_win, text='AnekdotNow', width=8,height=2, bg="white",fg="black", command=lambda: self.show_aneknow_joke()) 
            joke_aneknow_but.pack()
        self.show_win.mainloop()
        
    def show_saved_quote(self):
        self.show_win.destroy() #Убираем окно выбора
        self.show_qu_saved=Tk()
        self.show_qu_saved.title('Читаем цитаты: избранное')
        self.show_qu_saved.geometry('500x400+500+400')
        self.quote_saved=Text(self.show_qu_saved, width=80,height=24, wrap=WORD)
        self.quote_saved.insert(1.0, s.choose_quote_saved(2, pars.GetEndSaved()))
        self.but_quotes_back=Button(self.show_qu_saved, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.back_quote_saved())
        self.but_quotes_next=Button(self.show_qu_saved, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.next_quote_saved())
        self.but_quote_delete=Button(self.show_qu_saved, text='Удалить', width=8,height=1, bg="white",fg="black", command=lambda: self.delete_quote_saved())
        self.quote_saved.pack()
        self.but_quotes_back.place(x=100, y=366)
        self.but_quotes_next.place(x=300, y=366)
        self.but_quote_delete.place(x=400, y=366)
    def show_aneknow_joke(self):
        self.show_win.destroy() #Убираем окно выбора
        self.show_jo_aneknow=Tk()
        self.show_jo_aneknow.title('Читаем цитаты: ithappens')
        self.show_jo_aneknow.geometry('500x400+500+400')
        self.joke_aneknow=Text(self.show_jo_aneknow, width=80,height=24, wrap=WORD)
        self.joke_aneknow.insert(1.0, s.choose_joke_aneknow(2, pars.GetEndAnekdotnow()))
        self.but_joke_back=Button(self.show_jo_aneknow, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.back_joke('aneknow'))
        self.but_joke_next=Button(self.show_jo_aneknow, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.next_joke('aneknow'))
        self.but_joke_save=Button(self.show_jo_aneknow, text='Сохранить', width=8,height=1, bg="white",fg="black", command=lambda: self.save_joke('aneknow'))
        self.joke_aneknow.pack()
        self.but_joke_back.place(x=100, y=366)
        self.but_joke_next.place(x=300, y=366)
    def show_ithapp_quote(self):
        self.show_win.destroy() #Убираем окно выбора
        self.show_qu_ithapp=Tk()
        self.show_qu_ithapp.title('Читаем цитаты: ithappens')
        self.show_qu_ithapp.geometry('500x400+500+400')
        self.quote_ithapp=Text(self.show_qu_ithapp, width=80,height=24, wrap=WORD)
        self.quote_ithapp.insert(1.0, s.choose_quote_ithapp(2, pars.GetEndITHAPPENS_RU()))
        self.but_quote_back=Button(self.show_qu_ithapp, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.back_quote_ithapp())
        self.but_quote_next=Button(self.show_qu_ithapp, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.next_quote_ithapp())
        self.but_quote_save=Button(self.show_qu_ithapp, text='Сохранить', width=8,height=1, bg="white",fg="black", command=lambda: self.save_quote('ithappens'))
        self.quote_ithapp.pack()
        self.but_quote_back.place(x=100, y=366)
        self.but_quote_next.place(x=300, y=366)
    def show_bash_quote(self):  #Будут функции для шуток и цитат, длинные if блоки отменяются 
        self.show_win.destroy() #Убираем окно выбора
        self.show_qu_bash=Tk() #Создали новое окно
        self.show_qu_bash.title('Читаем цитаты: bash.im')
        self.show_qu_bash.geometry('500x400+500+400')
        self.quote_bash=Text(self.show_qu_bash, width=80,height=24, wrap=WORD)
        self.b=pars.GetEndBASH_IM()
        self.quote_bash.insert(1.0, s.choose_quote_bash(2, self.b))
        self.but_quote_back=Button(self.show_qu_bash, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.back_quote_bash())
        self.but_quote_next=Button(self.show_qu_bash, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.next_quote_bash())
        self.but_quote_save=Button(self.show_qu_bash, text='Сохранить', width=8,height=1, bg="white",fg="black", command=lambda: self.save_quote('bashim'))
        self.quote_bash.pack()
        self.but_quote_back.place(x=100, y=366)
        self.but_quote_next.place(x=300, y=366)
        self.but_quote_save.place(x=400, y=366)
    def save_quote(self, site):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone()
        c.execute('INSERT INTO quotes VALUES (null, ?, ?, ?)', (site, s.number, pars.line))
        conn.commit()
    def delete_quote_saved(self):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone()
        c.execute('DELETE FROM quotes WHERE id=?', (str(s.number)))
        conn.commit()
        self.quote_saved.delete(1.0, END)
        self.quote_saved.insert(1.0, s.choose_quote_saved(1, s.number))
    def back_quote(self):
        self.quote_saved.delete(1.0, END)
        self.quote_saved.insert(1.0, s.choose_quote_saved(0, s.number))
    def next_quote(self):
        self.quote_saved.delete(1.0, END)
        self.quote_saved.insert(1.0, s.choose_quote_saved(1, s.number))
        #Сделать единую функцию для всего
    def back_quote_ithapp(self):
        self.quote_ithapp.delete(1.0, END)
        self.quote_ithapp.insert(1.0, s.choose_quote_ithapp(0, s.number))
    def next_quote_ithapp(self):
        self.quote_ithapp.delete(1.0, END)
        self.quote_ithapp.insert(1.0, s.choose_quote_ithapp(1, s.number))
    def back_quote_bash(self):
        self.quote_bash.delete(1.0, END)
        self.quote_bash.insert(1.0, s.choose_quote_bash(0, s.number))
    def next_quote_bash(self):
        self.quote_bash.delete(1.0, END)
        self.quote_bash.insert(1.0, s.choose_quote_bash(1, s.number))
    def next_joke(self, site):
        if site=='aneknow':
            self.joke_aneknow.delete(1.0, END)
            self.joke_aneknow.insert(1.0, s.choose_joke_aneknow(1, s.number))
    def back_joke(self, site):
        if site=='aneknow':
            self.joke_aneknow.delete(1.0, END)
            self.joke_aneknow.insert(1.0, s.choose_joke_aneknow(0, s.number))
        
    def start(self):
        global root
        root = Tk()
        root.title('Цитаты и анекдоты')
        root.geometry('300x200+300+200') 
        root.resizable(True, False)
        quote_but=Button(root, text='Цитаты', width=8,height=2, bg="white",fg="black", command=lambda: self.what_window(0))
        joke_but=Button(root, text='Анекдоты', width=8,height=2, bg="white",fg="black", command=lambda: self.what_window(1))
        self.about_but=Button(root, text='О программе', width=9,height=2, bg="white",fg="black", command=lambda: self.about())
        now_lab=Label(root,text='Тестовая версия это')
        quote_but.pack()
        joke_but.pack()
        self.about_but.pack()
        now_lab.place(x=0, y=180)
        root.mainloop()
    def about(self):
        about=Tk()
        about.title('О программе')
        about.geometry('300x100+350+250') 
        about_label=Label(about,text='Анекдоты и цитаты 2013.\nАвтор - Grigory Smirnov и Vsevolod Alexandrov\nВерсия 0.6. ')
        about_exit_but=Button(about, text='Выход', width=9,height=1, bg="white",fg="black", command=lambda: about.destroy())
        about_label.pack()
        about_exit_but.pack()

pars=parser()   
s=choose()  
g=gui()
g.start()



