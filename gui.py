#Версия 1.0 от 22.12.2013.
#импорты и прочее
from main import parser as pars
from main import choose as s
from main import saver_n_other as sav
from tkinter import *
#Это gui оболочка. 
#Да, Tkinter плох.

class gui():
    def what_window(self, what): #0 - цитаты, 1 - шутки, 2 - о программе
        root.destroy() #Удаляем root окно
        self.show_win=Tk()
        self.show_win.geometry('300x200+300+200') 
        if what==0:
            self.show_win.title('Цитаты')
            quote_bash_but=Button(self.show_win, text='Bash.im', width=8,height=2, bg="white",fg="black", command=lambda: self.show_bash_quote())
            quote_ithapp_but=Button(self.show_win, text='IThappens', width=8,height=2, bg="white",fg="black", command=lambda: self.show_ithapp_quote()) 
            quote_saved_but=Button(self.show_win, text='Избранное', width=8,height=2, bg="white",fg="black", command=lambda: self.show_saved_quote())
            self.but_main_menu=Button(self.show_win, text='В меню', width=8,height=1, bg="white",fg="black", command=lambda: g.start(froms=self.show_win))
            quote_bash_but.pack()
            quote_ithapp_but.pack()
            quote_saved_but.pack()
            self.but_main_menu.pack()
        if what==1:
            self.show_win.title('Анекдоты')
            joke_aneknow_but=Button(self.show_win, text='AnekdotNow', width=8,height=2, bg="white",fg="black", command=lambda: self.show_joke_aneknow()) 
            joke_saved_but=Button(self.show_win, text='Избранное', width=8,height=2, bg="white",fg="black", command=lambda: self.show_saved_joke())
            self.but_main_menu=Button(self.show_win, text='В меню', width=8,height=1, bg="white",fg="black", command=lambda: g.start(froms=self.show_win))
            joke_aneknow_but.pack()
            joke_saved_but.pack()
            self.but_main_menu.pack()
        self.show_win.mainloop()
    #Сделать единое окно для показа шуток и цитат
    def show_saved_quote(self):
        self.show_win.destroy() #Убираем окно выбора
        self.show_qu_saved=Tk()
        self.show_qu_saved.title('Читаем цитаты: избранное')
        self.show_qu_saved.geometry('500x400+500+400')
        self.quote_board=Text(self.show_qu_saved, width=80,height=22, wrap=WORD, font='Ubuntu 11')
        self.nquote=pars.GetEndSaved(pars)
        self.quote=s.choose_quote_saved(s, 2, self.nquote)
        self.quote_board.insert(1.0, self.quote)
        self.but_quotes_back=Button(self.show_qu_saved, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.move_quote(s.choose_quote_saved(s, 0, s.number)))
        self.but_quotes_next=Button(self.show_qu_saved, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.move_quote(s.choose_quote_saved(s, 1, s.number)))
        self.but_quote_delete=Button(self.show_qu_saved, text='Удалить', width=8,height=1, bg="white",fg="black", command=lambda: sav.delete_quote_saved(sav))
        self.but_main_menu=Button(self.show_qu_saved, text='В меню', width=8,height=1, bg="white",fg="black", command=lambda: g.start(froms=self.show_qu_saved))
        self.quote_board.pack()
        self.but_quotes_back.place(x=200, y=367)
        self.but_quotes_next.place(x=300, y=367)
        self.but_quote_delete.place(x=400, y=367)
        self.but_main_menu.place(x=100, y=367)
    def show_saved_joke(self):
        self.show_win.destroy() #Убираем окно выбора
        self.show_jo_saved=Tk()
        self.show_jo_saved.title('Читаем шутки: избранное')
        self.show_jo_saved.geometry('500x400+500+400')
        self.joke_board=Text(self.show_jo_saved, width=80,height=22, wrap=WORD, font='Ubuntu 11')
        self.njoke=pars.GetEndSavedJokes(pars)
        self.joke=s.choose_joke_saved(s, 2, self.njoke)
        self.joke_board.insert(1.0, self.joke)
        self.but_jokes_back=Button(self.show_jo_saved, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.move_joke(s.choose_joke_saved(s, 0, s.number)))
        self.but_jokes_next=Button(self.show_jo_saved, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.move_joke(s.choose_joke_saved(s, 1, s.number)))
        self.but_jokes_delete=Button(self.show_jo_saved, text='Удалить', width=8,height=1, bg="white",fg="black", command=lambda: sav.delete_joke_saved(sav))
        self.but_main_menu=Button(self.show_jo_saved, text='В меню', width=8,height=1, bg="white",fg="black", command=lambda: g.start(froms=self.show_jo_saved))
        self.joke_board.pack()
        self.but_jokes_back.place(x=200, y=367)
        self.but_jokes_next.place(x=300, y=367)
        self.but_jokes_delete.place(x=400, y=367)
        self.but_main_menu.place(x=100, y=367)
    def show_joke_aneknow(self):
        self.show_win.destroy() #Убираем окно выбора
        self.show_jo_aneknow=Tk()
        self.show_jo_aneknow.title('Читаем анекдоты: aneknow')
        self.show_jo_aneknow.geometry('500x400+500+400')
        self.joke_board=Text(self.show_jo_aneknow, width=80,height=22, wrap=WORD, font='Ubuntu 10')
        self.njoke=pars.GetEndAnekdotnow(pars)
        self.joke=s.choose_joke_aneknow(s, 2, self.njoke)
        self.joke_board.insert(1.0, self.joke)
        self.but_joke_back=Button(self.show_jo_aneknow, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.move_joke(s.choose_joke_aneknow(s, 0, s.number)))
        self.but_joke_next=Button(self.show_jo_aneknow, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.move_joke(s.choose_joke_aneknow(s, 1, s.number)))
        self.but_joke_save=Button(self.show_jo_aneknow, text='Сохранить', width=8,height=1, bg="white",fg="black", command=lambda: sav.save_joke(sav, 'anekdotsnow', s.number, self.joke))
        self.but_main_menu=Button(self.show_jo_aneknow, text='В меню', width=8,height=1, bg="white",fg="black", command=lambda: g.start(froms=self.show_jo_aneknow))
        self.but_random=Button(self.show_jo_aneknow, text='Случайная', width=8,height=1, bg="white",fg="black", command=lambda: self.random_joke(sav.random_joke(sav, 'aneknow')))
        self.joke_board.pack()
        self.but_joke_back.place(x=200, y=367)
        self.but_joke_next.place(x=300, y=366)
        self.but_joke_save.place(x=400, y=367)
        self.but_main_menu.place(x=000, y=367)
        self.but_random.place(x=100, y=367)
    def show_ithapp_quote(self):
        self.show_win.destroy() #Убираем окно выбора
        self.show_qu_ithapp=Tk()
        self.show_qu_ithapp.title('Читаем цитаты: ithappens')
        self.show_qu_ithapp.geometry('500x410+500+400')
        self.quote_board=Text(self.show_qu_ithapp, width=80,height=22, wrap=WORD, font='Ubuntu 10')
        self.nquote=pars.GetEndITHAPPENS_RU(pars)
        self.quote=s.choose_quote_ithapp(s, 2, self.nquote)
        self.quote_board.insert(1.0, self.quote)
        self.but_quote_back=Button(self.show_qu_ithapp, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.move_quote(s.choose_quote_ithapp(s, 0, s.number)))
        self.but_quote_next=Button(self.show_qu_ithapp, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.move_quote(s.choose_quote_ithapp(s, 1, s.number)))
        self.but_quote_save=Button(self.show_qu_ithapp, text='Сохранить', width=8,height=1, bg="white",fg="black", command=lambda: sav.save_quote(sav, 'ithappens', s.number, self.quote))
        self.but_main_menu=Button(self.show_qu_ithapp, text='В меню', width=8,height=1, bg="white",fg="black", command=lambda: g.start(froms=self.show_qu_ithapp))
        self.but_random=Button(self.show_qu_ithapp, text='Случайная', width=8,height=1, bg="white",fg="black", command=lambda: self.random_quote(sav.random_quote(sav, 'ithapp')))
        self.quote_board.pack()
        self.but_quote_back.place(x=200, y=367)
        self.but_quote_next.place(x=300, y=367)
        self.but_quote_save.place(x=400, y=367)
        self.but_main_menu.place(x=000, y=367)
        self.but_random.place(x=100, y=367)
    def show_bash_quote(self):  
        self.show_win.destroy() #Убираем окно выбора
        self.show_qu_bash=Tk() #Создали новое окно
        self.show_qu_bash.title('Читаем цитаты: bash.im')
        self.show_qu_bash.geometry('500x400+500+400')
        self.show_qu_bash.resizable(False, False)
        self.quote_board=Text(self.show_qu_bash, width=80,height=22, wrap=WORD, font='Ubuntu 10')
        self.nquote=pars.GetEndBASH_IM(pars)
        self.quote=s.choose_quote_bash(s, 2, self.nquote)
        self.quote_board.insert(1.0, self.quote)
        self.but_quote_back=Button(self.show_qu_bash, text='Назад', width=8,height=1, bg="white",fg="black", command=lambda: self.move_quote(s.choose_quote_bash(s, 0, s.number)))
        self.but_quote_next=Button(self.show_qu_bash, text='Вперед', width=8,height=1, bg="white",fg="black", command=lambda: self.move_quote(s.choose_quote_bash(s, 1, s.number)))
        self.but_quote_save=Button(self.show_qu_bash, text='Сохранить', width=8,height=1, bg="white",fg="black", command=lambda: sav.save_quote(sav, 'bashim', s.number, self.quote))
        self.but_main_menu=Button(self.show_qu_bash, text='В меню', width=8,height=1, bg="white",fg="black", command=lambda: g.start(froms=self.show_qu_bash))
        self.but_random=Button(self.show_qu_bash, text='Случайная', width=8,height=1, bg="white",fg="black", command=lambda: self.random_quote(sav.random_quote(sav, 'bashim')))
        self.quote_board.pack()
        self.but_quote_back.place(x=200, y=367)
        self.but_quote_next.place(x=300, y=367)
        self.but_quote_save.place(x=400, y=367)
        self.but_random.place(x=100, y=367)
        self.but_main_menu.place(x=000, y=367)
    def move_quote(self, site='Произошла ошибка 01q'):
        self.quote_board.delete(1.0, END)
        self.quote=site
        if self.quote==None:
            self.quote_board.insert(1.0, 'Дальше пусто :(\nПожалуйста, не нажимайте кнопку "Назад" или "Вперед" еще раз, если не хотите получить кучу ошибок и не возможность перехода к следующей цитате.')
        else:
            self.quote_board.insert(1.0, self.quote)
    def move_joke(self, site='Произошла ошибка 01j'):
        self.joke_board.delete(1.0, END)
        self.joke=site
        if self.joke==' ' or self.joke==None:
            self.joke_board.insert(1.0, 'Дальше пусто :(\nПожалуйста, не нажимайте кнопку "Назад" или "Вперед" еще раз, если не хотите получить кучу ошибок и не возможность перехода к следующей цитате.')
        else:
            self.joke_board.insert(1.0, self.joke)
    def random_quote(self, site='Произошла ошибка 01qr'):
        self.quote_board.delete(1.0, END)
        self.quote=site
        if self.quote==None:
            self.quote_board.insert(1.0, 'Дальше пусто :(\nПожалуйста, не нажимайте кнопку "Назад" или "Вперед" еще раз, если не хотите получить кучу ошибок и не возможность перехода к следующей цитате.')
        else:
            self.quote_board.insert(1.0, self.quote)
    def random_joke(self, site='Произошла ошибка 01qr'):
        self.joke_board.delete(1.0, END)
        self.joke=site
        if self.joke==None:
            self.joke_board.insert(1.0, 'Дальше пусто :(\nПожалуйста, не нажимайте кнопку "Назад" или "Вперед" еще раз, если не хотите получить кучу ошибок и не возможность перехода к следующей цитате.')
        else:
            self.joke_board.insert(1.0, self.joke)
    def start(self, froms=None): #froms нужен для закрытия прошлого окна
        try:
            froms.destroy()
        except:
            pass
        global root
        root = Tk()
        root.title('Цитаты и анекдоты')
        root.geometry('300x200+300+200') 
        root.resizable(True, False)
        quote_but=Button(root, text='Цитаты', width=8,height=2, bg="white",fg="black", command=lambda: self.what_window(0))
        joke_but=Button(root, text='Анекдоты', width=8,height=2, bg="white",fg="black", command=lambda: self.what_window(1))
        self.about_but=Button(root, text='О программе', width=9,height=2, bg="white",fg="black", command=lambda: self.about())
        now_lab=Label(root,text='Версия 1.0.')
        quote_but.pack()
        joke_but.pack()
        self.about_but.pack()
        now_lab.place(x=0, y=180)
        root.mainloop()
    def about(self):
        about=Tk()
        about.title('О программе')
        about.geometry('470x120+350+250') 
        about_label=Label(about,text='Quotes&Jokes 2013.\nАвтор - Grigory Smirnov\nВерсия 1.0 от 22.12.2013.\nЗа модули парсинга и другую помощь благодарим Vsevolod Alexsandrov.')
        about_label_cont=Label(about,text='Автор ВКонтакте: vk.com/grigorys\nТакже предлагаем посетить dafter.ru')
        about_exit_but=Button(about, text='Выход', width=9,height=1, bg="white",fg="black", command=lambda: about.destroy())
        about_label.pack()
        about_label_cont.pack()
        about_exit_but.pack()
g=gui()
g.start()

#01q - ошибка добавления цитаты в quote_board
#01j - ошибка добавления цитаты в joke_board
#01qr - ошибка добавления рандомной цитаты
