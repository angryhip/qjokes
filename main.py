#Grigory Smirnov 2013
#There use sevenid modules (parsing)
#This code is protected by copyright. Copying is prohibited! Distribution of the source code is prohibited!
from tkinter import *
import sqlite3
from urllib.request import Request, urlopen
import random
class parser():
    def replace(self, line, repl, replto):

        nin2 = len(repl)
        nin1 = line.find(repl)
        while nin1 != -1:
            nin3 = nin1 + nin2
            line = line[:nin1]+replto+line[nin3:]
            nin1 = line.find(repl)
        return(line)
    def GetRequest(self, url):
        try:
            requ = urlopen(Request(url, headers={'User-Agent': 'Mozilla/5.0'})).read()
        except: 
            print ("По мистическим причинам связь не установлена. Скорее всего у вас нет интернет подключения :(")
            exit(-1)
        return(requ)
    def GetEndSaved(self):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone() 
        try:
            self.QuotesNumber=self.lastid[0]
            return(self.lastid[0])
        except:
            return('Пусто')
    def GetQuoteSaved(self, number):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone() 
        try:
            self.QuotesNumber=self.lastid[0]
        except:
            return('Тут пусто :(\nДобавить цитаты в избранное можно в окне чтения.')
        try:
            c.execute('SELECT text FROM quotes WHERE id=?', (str(number)))
            self.tmp=c.fetchone()  
        except:
            return s.choose_quote_saved(s, 2, pars.GetEndSaved())
        if self.tmp == None:
            return None
        else:
            return(self.tmp[0])
    def GetEndSavedJokes(self):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists jokes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM jokes''')
        self.lastid=c.fetchone() 
        try:
            self.QuotesNumber=self.lastid[0]
            return(self.lastid[0])
        except:
            return('Пусто')
    def GetJokeSaved(self, number):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists jokes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM jokes''')
        self.lastid=c.fetchone() 
        try:
            self.JokesNumber=self.lastid[0]
        except:
            return('Тут пусто :(\nДобавить шутки в избранное можно в окне чтения.')
        try:
            c.execute('SELECT text FROM jokes WHERE id=?', (str(number)))
            self.tmp=c.fetchone()  
        except:
            return s.choose_joke_saved(s, 2, pars.GetEndSavedJokes())
        if self.tmp == None:
            return None
        else:
            return(self.tmp[0])
    def GetEndBASH_IM(self):
        line = pars.GetRequest('http://bash.im').decode("cp1251")
        line = line.split('\n', 31)[30]
        line = line[83:-31]
        return(int(line))
    def GetEndAnekdotnow(self):
        line = pars.GetRequest('http://anekdotnow.ru').decode("cp1251")
        line = line[(line.find("<td><img src='img/forum.jpg' height='16'></td>\n<td ><a href='/funny.php?t=papa-papa-day-deneg")+93):]
        line = line[(line.find("&k=")+3):]
        line = line[:(line.find("'>"))]
        line = int(line)
        return(line)
    def GetEndITHAPPENS_RU(self):
        line = (pars.GetRequest('http://ithappens.ru/rss/').decode("cp1251"))
        line = line.split('\n', 13)[12]
        line = line[54:(line.find("</guid>"))]
        return(int(line))
    def GetJokeAnekdotnow(self, number):
        url = 'http://anekdotnow.ru/funny.php?k=' + str(number)
        line = pars.GetRequest(url).decode("cp1251")
        line = line[(line.find("<br><center><img src='img/bullet2.gif'></center><br>")+53):(line.find("<br clear='all'>"))]
        line = pars.replace(line, '\t'*4, ' ')
        line = ' '+line[6:]
        line = pars.replace(line, '<br />', '')
        line = pars.replace(line, '&quot;', '"')
        return(line)
    def GetQuoteBASH_IM(self, nquote):
        url = 'http://bash.im/quote/' + str(nquote)
        iline = self.GetRequest(url).decode('cp1251')
        line = iline[(iline.find('<meta property="og:description" content="')+41):]
        line = line[:(line.find('" />'))]
        line = pars.replace(line, '&#13;&#10;', '\n')
        line = pars.replace(line, '&quot;', '"')
        line = pars.replace(line, '&lt;', '<')
        line = pars.replace(line, '&gt;', '>')
        quote = line
        line = iline[(iline.find('class="rating">')+15):(iline.find('</span></span>'))]
        return(quote)
    def GetQuoteITHAPPENS_RU(self, nquote): #nquote - номер цитаты
        url = 'http://ithappens.ru/story/' + str(nquote)
        line = self.GetRequest(url).decode('cp1251')
        line = line[(line.find('<p class="text" id="')+20):]
        line = line[(line.find('">')+2):]
        line = line[:(line.find('</p>'))]
        line = self.replace(line, '&nbsp;', ' ')
        line = self.replace(line, '<br>', '\n')
        return(line)
class choose():
    def choose_quote_bash(self, back, number):
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            if pars.GetQuoteBASH_IM(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndBASH_IM()
                return(pars.GetQuoteBASH_IM(int(self.number)))
            else:
                return(pars.GetQuoteBASH_IM(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            if pars.GetQuoteBASH_IM(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndBASH_IM()
                return(pars.GetQuoteBASH_IM(int(self.number)))
                g.but_quote_back.destroy()
                g.but_quote_back.pack()
            else:
                quote=pars.GetQuoteBASH_IM(int(self.number))
                return(quote)
        elif back==2:
            number=pars.GetEndBASH_IM()
            return(pars.GetQuoteBASH_IM(number))
        elif back==3:
            return(pars.GetQuoteBASH_IM(int(self.number)))
    def choose_joke_aneknow(self, back, number):
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            if pars.GetJokeAnekdotnow(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndAnekdotnow()
                return(pars.GetJokeAnekdotnow(int(self.number)))
            else:
                return(pars.GetJokeAnekdotnow(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            if pars.GetJokeAnekdotnow(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndAnekdotnow()
                return(pars.GetJokeAnekdotnow(int(self.number)))
                g.but_joke_back.destroy()
                g.but_joke_back.pack()
            else:
                return(pars.GetJokeAnekdotnow(int(self.number)))
        elif back==2:
            number=pars.GetEndAnekdotnow()
            return(pars.GetJokeAnekdotnow(number))
    def choose_quote_ithapp(self, back, number):
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            if pars.GetQuoteITHAPPENS_RU(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndITHAPPENS_RU()
                a=pars.GetQuoteITHAPPENS_RU(int(self.number))
                return(a)
            else:
                return(pars.GetQuoteITHAPPENS_RU(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            if pars.GetQuoteITHAPPENS_RU(int(self.number))=='pe="text/css" rel="stylesheet':
                self.number=pars.GetEndITHAPPENS_RU()
                return(pars.GetQuoteITHAPPENS_RU(int(self.number)))
                g.but_quote_back.destroy()
                g.but_quote_back.pack()
            else:
                return(pars.GetQuoteITHAPPENS_RU(int(self.number)))
        elif back==2:
            number=pars.GetEndITHAPPENS_RU()
            return(pars.GetQuoteITHAPPENS_RU(number))
    def choose_quote_saved(self, back, number): 
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            return(pars.GetQuoteSaved(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            return(pars.GetQuoteSaved(int(self.number)))
        elif back==2:
            number=pars.GetEndSaved()
            return(pars.GetQuoteSaved(number))
    def choose_joke_saved(self, back, number): 
        self.number=number
        if back==0: #0-back, 1-next
            self.number=int(self.number)-1
            return(pars.GetJokeSaved(int(self.number)))
        elif back==1:
            self.number=int(self.number)+1
            return(pars.GetJokeSaved(int(self.number)))
        elif back==2:
            number=pars.GetEndSavedJokes()
            return(pars.GetJokeSaved(number))
class saver_n_other():
    def save_quote(self, site, number, content):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone()
        c.execute('INSERT INTO quotes VALUES (null, ?, ?, ?)', (site, number, content))
        conn.commit()
    def save_joke(self, site, number, content):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists jokes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM jokes''')
        self.lastid=c.fetchone()
        c.execute('INSERT INTO jokes VALUES (null, ?, ?, ?)', (site, number, content))
        conn.commit()
    def random_quote(self, site):
        if site=='bashim':
            self.qunumb=pars.GetEndBASH_IM()
            self.randqunumb=random.randint(1, int(self.qunumb))
            return(pars.GetQuoteBASH_IM(self.randqunumb))
        elif site=='ithapp':
            self.qunumb=pars.GetEndITHAPPENS_RU()
            self.randqunumb=random.randint(1, int(self.qunumb))
            return(pars.GetQuoteITHAPPENS_RU(self.randqunumb))
    def random_joke(self, site):
        if site=='aneknow':
            self.jonumb=pars.GetEndAnekdotnow()
            self.randjonumb=random.randint(1, int(self.jonumb))
            return(pars.GetJokeAnekdotnow(self.randjonumb))
    def delete_quote_saved(self):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists quotes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM quotes''')
        self.lastid=c.fetchone()
        c.execute('DELETE FROM quotes WHERE id=?', (str(s.number)))
        conn.commit()
        return('Удалено.')
    def delete_joke_saved(self):
        conn=sqlite3.connect('quotes-jokes.db')
        c = conn.cursor() 
        c.execute('''create table if not exists jokes (id INTEGER PRIMARY KEY, site, number, text TEXT)''')
        c.execute('''SELECT id FROM jokes''')
        self.lastid=c.fetchone()
        c.execute('DELETE FROM jokes WHERE id=?', (str(s.number)))
        conn.commit()
        return('Удалено.')
s=choose()
pars=parser()
